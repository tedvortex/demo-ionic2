# demo-ionic2

## Usage

Install Ionic and Cordova globally:

    # npm i -g ionic@beta cordova

Clone the app:

    $ git clone git@bitbucket.org:continer/demo-ionic2.git
    $ cd demo-ionic2

Setup and run:

    $ npm i
    $ ionic serve
