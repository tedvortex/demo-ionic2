import {NavController, Page, ActionSheet} from 'ionic-angular';
import {BusinessData} from '../../providers/business-data';
import {UserDetailPage} from '../user-detail/user-detail';


@Page({
  templateUrl: 'build/pages/user-list/user-list.html'
})
export class UserListPage {
  static get parameters() {
    return [[NavController], [BusinessData]];
  }

  constructor(nav, businessData) {
    this.nav = nav;
    this.businessData = businessData;
    this.users = [];

    businessData.getUsers().then(users => {
      this.users = users;
    });
  }

  goToUserDetail(UserName) {
    this.nav.push(UserDetailPage, UserName);
  }

  goToUserTwitter(User) {
    window.open(`https://twitter.com/${User.twitter}`);
  }

  openUserShare(User) {
    let actionSheet = ActionSheet.create({
      title: 'Share ' + User.name,
      buttons: [
        {
          text: 'Copy Link',
          handler: () => {
            console.log("Copy link clicked on https://twitter.com/" + User.twitter);
            if (window.cordova && window.cordova.plugins.clipboard) {
              window.cordova.plugins.clipboard.copy("https://twitter.com/" + User.twitter);
            }
          }
        },
        {
          text: 'Share via ...',
          handler: () => {
            console.log("Share via clicked");
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });

    this.nav.present(actionSheet);
  }
}
