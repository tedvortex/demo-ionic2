import {IonicApp, Page, Modal, Alert, NavController} from 'ionic-angular';
import {BusinessData} from '../../providers/business-data';
import {UserData} from '../../providers/user-data';
import {ProjectDetailPage} from '../project-detail/project-detail';


@Page({
    templateUrl: 'build/pages/project-list/project-list.html'
})
export class ProjectListPage {
    static get parameters() {
        return [[IonicApp], [NavController], [BusinessData], [UserData]];
    }

    constructor(app, nav, businessData, user) {
        this.app = app;
        this.nav = nav;
        this.businessData = businessData;
        this.user = user;
        this.shownProjects = 0;

        businessData.getProjects().then(projects => {
            this.projects = projects;
            this.shownProjects = projects.length;
        });
    }

    onPageDidEnter() {
        this.app.setTitle('Projects');
    }

    goToProjectDetail(projectData) {
        // go to the session detail page
        // and pass in the session data
        this.nav.push(ProjectDetailPage, projectData);
    }

}
