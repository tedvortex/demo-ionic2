import {Page, NavParams} from 'ionic-angular';


@Page({
    templateUrl: 'build/pages/project-detail/project-detail.html'
})
export class ProjectDetailPage {
    static get parameters() {
        return [[NavParams]];
    }

    constructor(navParams) {
        this.navParams = navParams;
        this.project = navParams.data;
    }
}
