import {NavController, NavParams, Page} from 'ionic-angular';


@Page({
  templateUrl: 'build/pages/user-detail/user-detail.html'
})
export class UserDetailPage {
  static get parameters() {
    return [[NavController], [NavParams]];
  }

  constructor(nav, navParams) {
    this.nav = nav;
    this.navParams = navParams;
    this.user = this.navParams.data;
  }
}
