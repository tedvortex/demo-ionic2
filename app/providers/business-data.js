import {Injectable} from 'angular2/core';
import {Http} from 'angular2/http';
import {UserData} from './user-data';


@Injectable()
export class BusinessData {
    static get parameters(){
        return [[Http], [UserData]];
    }

    constructor(http, user) {
        // inject the Http provider and set to this instance
        this.http = http;
        this.user = user;
    }

    load() {
        if (this.data) {
            // already loaded data
            return Promise.resolve(this.data);
        }

        // don't have the data yet
        return new Promise(resolve => {
            // We're using Angular Http provider to request the data,
            // then on the response it'll map the JSON data to a parsed JS object.
            // Next we process the data and resolve the promise with the new data.
            this.http.get('data/data.json').subscribe(res => {
                // we've got back the raw data, now generate the core schedule data
                // and save the data for later reference
                this.data = this.processData(res.json());
                resolve(this.data);
            });
        });
    }

    processData(data) {
        return data;
    }

    getProjects() {
        return this.load().then(data => {
            return data.projects;
        });
    }

    getUsers() {
        return this.load().then(data => {
            return data.users.sort((a, b) => {
                let aName = a.name.split(' ').pop();
                let bName = b.name.split(' ').pop();
                return aName.localeCompare(bName);
            });
        });
    }

    getMap() {
        return this.load().then(data => {
            return data.map;
        });
    }

}
